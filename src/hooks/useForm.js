import { useState } from 'react';

const useFormFields = () => {
  const [fields, setFields] = useState({});

  const handleFieldChange = (fieldName, value) => {
    setFields((prevFields) => ({
      ...prevFields,
      [fieldName]: value,
    }));
  };

  const getField = (fieldName) => {
    return fields[fieldName] || '';
  };

  const setField = (fieldName, value) => {
    setFields((prevFields) => ({
      ...prevFields,
      [fieldName]: value,
    }));
  };

  const resetFields = () => {
    setFields({});
  };

  return { fields, handleFieldChange, getField, setField, resetFields };
};

export default useFormFields;
