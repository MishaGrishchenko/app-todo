import { useState } from 'react';

const useErrorHandling = () => {
  const [errors, setErrors] = useState({});

  const setError = (field, errorMessage) => {
    setErrors(prevErrors => ({
      ...prevErrors,
      [field]: errorMessage,
    }));
  };

  const clearError = (field) => {
    setErrors(prevErrors => ({
      ...prevErrors,
      [field]: '',
    }));
  };

  const resetErrors = () => {
    setErrors({});
  };

  return {
    errors,
    setError,
    clearError,
    resetErrors,
  };
};

export default useErrorHandling;
