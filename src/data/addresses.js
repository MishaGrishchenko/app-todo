const addresses = [
  { value: 'address1', label: 'Address 1' },
  { value: 'address2', label: 'Address 2' },
  { value: 'address3', label: 'Address 3' },
  { value: 'address4', label: 'Address 4' },
  { value: 'address5', label: 'Address 5' },
  { value: 'address6', label: 'Address 6' },
  { value: 'address7', label: 'Address 7' },
  { value: 'address8', label: 'Address 8' },
  { value: 'address9', label: 'Address 9' },
  { value: 'address10', label: 'Address 10' },
];

export default addresses;
