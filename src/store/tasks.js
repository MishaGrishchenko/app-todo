import { createSlice } from '@reduxjs/toolkit';

export const tasksSlice = createSlice({
  name: 'tasks',
  initialState: {
    value: [],
    error: '',
    success: '',
  },
  reducers: {
    addTask: (state, action) => {
      const { email } = action.payload;

      const existingTask = state.value.find(task => task.email === email);
      if (existingTask) {
        return { ...state, error: 'Task with this email already exists.' };
      }

      const newId = Date.now() + Math.random();
      const newTask = { ...action.payload, id: newId };
      return { ...state, value: [...state.value, newTask], error: '', success: 'Task added successfully. 👍' };
    },
    deleteTask: (state, action) => {
      const idToDelete = action.payload;
      return { ...state, value: state.value.filter(task => task.id !== idToDelete), error: '' };
    },
    clearError: (state) => {
      return { ...state, error: '' };
    },
    clearSuccess: (state) => {
      return { ...state, success: '' };
    },
  },
});

export const { addTask, deleteTask, clearError, clearSuccess } = tasksSlice.actions;

export default tasksSlice.reducer;
