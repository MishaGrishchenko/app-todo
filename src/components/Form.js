import React, { useState } from 'react';
import ErrorMessage from './ErrorMessage';
import Button from './Button';
import UseForm from '../hooks/useForm';
import useErrorHandling from '../hooks/useError';
import addresses from '../data/addresses'

const AddressForm = ({ func }) => {
  const { getField, setField, resetFields } = UseForm();
  const { errors, setError, clearError, resetErrors } = useErrorHandling();
  const [submitError, setSubmitError] = useState('');

  const reset = () => {
    resetFields();
    resetErrors();
    setSubmitError('');
  };

  const handleAddressChange = (event) => {
    setField('selectedAddresses', event.target.value);
  };

  const handleSubmit = (event) => {
    event.preventDefault();
    let hasError = false;

    if (!getField('selectedAddresses')) {
      setError('selectedAddresses', 'Enter address');
      hasError = true;
    } else {
      clearError('selectedAddresses');
    }

    if (!getField('name')) {
      setError('name', 'Enter your name');
      hasError = true;
    } else {
      clearError('name');
    }

    if (!getField('phone')) {
      setError('phone', 'Enter your phone');
      hasError = true;
    } else {
      clearError('phone');
    }

    if (!getField('email')) {
      setError('email', 'Enter your email');
      hasError = true;
    } else if (!validateEmail(getField('email'))) {
      setError('email', 'Enter a correct email');
      hasError = true;
    } else {
      clearError('email');
    }

    if (hasError) {
      setSubmitError('The form contains errors. Please correct and try again.');
      return;
    }

    func({
      address: getField('selectedAddresses'),
      name: getField('name'),
      phone: getField('phone'),
      email: getField('email'),
    })

    reset();
  };

  const validateEmail = (email) => {
    const regex = /^[^\s@]+@[^\s@]+\.[^\s@]+$/;
    return regex.test(email);
  };

  return (
    <form onSubmit={handleSubmit} className='form'>
        <div className='items'>
          <label>Address:</label>
          <select value={getField('selectedAddresses' || '')} onChange={handleAddressChange}>
            <option value=""></option>
            {addresses.map((address) => (
              <option key={address.value} value={address.value}>{address.label}</option>
            ))}
          </select>
          <ErrorMessage message={errors.selectedAddresses} />
        </div>
        <div className='items'>
          <label>Name:</label>
          <input type="text" value={getField('name' || '')} onChange={(e) => setField('name', e.target.value)} />
          <ErrorMessage message={errors.name} />
        </div>
        <div className='items'>
          <label>Phone:</label>
          <input
            type="tel"
            value={getField('phone' || '')}
            onChange={(e) => setField('phone', e.target.value)}
            pattern="[0-9]{3}[0-9]{3}[0-9]{4}"
            title="Enter your phone number in the format XXX-XXX-XXXX"
          />
          <ErrorMessage message={errors.phone} />
        </div>
        <div className='items'>
          <label>Email:</label>
          <input type="email" value={getField('email' || '')} onChange={(e) => setField('email', e.target.value)} />
          <ErrorMessage message={errors.email} />
        </div>
        <div className='actions'>
          <Button isPrimary={true} onClick={handleSubmit}>Send</Button>
          <Button isPrimary={false} onClick={reset} disabled={!submitError}>Clear</Button>
        </div>
        {submitError && <ErrorMessage message={submitError} />}
    </form>
  );
};

export default AddressForm;
