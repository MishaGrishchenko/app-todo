import React from 'react';

const Card = ({ address, name, phone, email, func }) => {
  return (
    <div className='task-item'>
      <div>{address}</div>
      <div>{name}</div>
      <div>{phone}</div>
      <div>{email}</div>
      <div className='close' onClick={() => func()}>&#x2715;</div>
    </div>
  )
}

export default Card;