import React from 'react';


const Button = React.memo(({ isPrimary, onClick, children, disabled }) => {
  const classBtn = isPrimary ? 'primary' : 'secondary';

  return (
    <button className={`button ${classBtn}`} onClick={onClick} disabled={disabled}>
      {children}
    </button>
  );
});

export default Button;
