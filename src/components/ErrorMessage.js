import React from 'react';

const ErrorMessage = ({ message }) => {
  return (
    <div style={{ color: 'red', margin: '2px 0 10px' }}>
      {message}
    </div>
  );
};

export default ErrorMessage;
