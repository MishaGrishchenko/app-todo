import React, { useState, useEffect } from 'react';
import Form from './components/Form';
import Card from './components/Card';
import './App.css';
import { useSelector, useDispatch } from 'react-redux';
import { addTask, deleteTask } from './store/tasks';

function App() {
  const tasks = useSelector((state) => state.tasks.value);
  const errorMess = useSelector((state) => state.tasks.error);
  const successMess = useSelector((state) => state.tasks.success);
  const dispatch = useDispatch();
  const [showMessage, setShowMessage] = useState(false);

  useEffect(() => {
    if (errorMess || successMess) {
      setShowMessage(true);
      setTimeout(() => {
        setShowMessage(false);
      }, 4000);
    }
  }, [errorMess, successMess]);

  const handleAddTask = (task) => {
    dispatch(addTask(task));
  };

  const handleDeleteTask = (id) => {
    dispatch(deleteTask(id));
  };

  return (
    <div className="App">
      <div className="container">
        <Form func={handleAddTask} />
        {showMessage && (errorMess ? (
          <div className='message message-error'>{errorMess}</div>
        ) : (
          <div className='message message-success'>{successMess}</div>
        ))}
        <div className="task-items">
          {tasks && tasks.map((task, index) => (
            <Card key={task.id}
              address={task.address}
              name={task.name}
              phone={task.phone}
              email={task.email}
              func={() => handleDeleteTask(task.id)}
            />
          ))}
        </div>
      </div>
    </div>
  );
}

export default App;
