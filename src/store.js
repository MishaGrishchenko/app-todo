import { configureStore } from '@reduxjs/toolkit'
import tasksSlice from './store/tasks'

export default configureStore({
  reducer: {
    tasks: tasksSlice
  },
})